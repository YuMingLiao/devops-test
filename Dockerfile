FROM ubuntu:16.04

RUN apt update && apt install -y \
        apache2

COPY index.html /var/www
COPY 000-default.conf /etc/apache2/sites-enabled
COPY apache2.conf /etc/apache2/
COPY ports.conf /etc/apache2/

EXPOSE 5000

CMD ["apachectl", "-D", "FOREGROUND"]